DEFAULT_PORT_PREFIX="80"
DEFAULT_JPDA_ADDRESS_PREFIX="83"
DEFAULT_PORT="81"
DEFAULT_COMMAND="start"
BASE_SRC="/data/www/src"
DEFUALT_SRC="afbx"
APP_PREFIX="afbx-"
DEFAULT_APP="rest"
INSTANCE_PREFIX="afbx-80"
BASE_APP_PATH="/data/www/apps/"
DEFAULT_BRANCH="master"

APP_PORT=$DEFAULT_PORT_PREFIX$DEFAULT_PORT
JPDA_PORT=$DEFAULT_JPDA_ADDRESS_PREFIX$DEFAULT_PORT
APP_NAME=$APP_PREFIX$DEFAULT_APP
COMMAND=$DEFAULT_COMMAND
SRC=$DEFUALT_SRC
RESTART=0
JPDA=1
INSTANCE=$INSTANCE_PREFIX$DEFAULT_PORT
BRANCH=$DEFAULT_BRANCH

help()
{
  echo "-p:部署实例的端口，默认81,完整端口是：8081"
  echo "-n:部署项目的名称，如rest,admin,task,默认rest"
  echo "-c:启动关闭tomcat服务命令，如deploy,start,stop，默认start"
  echo "-s:源码目录，默认主干,afbx"
  echo "-r:不进行更新源码编译，直接重启,1:重启，0：不重启，默认0"
  echo "-d:jpda方式启动"
  echo "-b:分支名称，默认master"
  echo "-h:帮助信息"
  exit 0
}

idx=1000
currentDay=`date '+%Y%m%d'`
function ft ()
{
idx=1000
#local base="/data/www/apps/afbx-8081"
local base=$1
while [ -d $base/$currentDay$idx ]
do
  let "idx+=1"
done
}

while [ -n "$1" ]; do
case "$1" in
  -h)help; shift 1;;
  -p)APP_PORT=$DEFAULT_PORT_PREFIX$2; JPDA_PORT=$DEFAULT_JPDA_ADDRESS_PREFIX$2; INSTANCE=$INSTANCE_PREFIX$2; shift 2;;
  -n)APP_NAME=$APP_PREFIX$2; shift 2;;
  -c)COMMAND=$2; shift 2;;
  -s)SRC=$2; shift 2;;
  -r)RESTART=$2; shift 2;;
  -d)JPDA=$2; shift 2;;
  -b)BRANCH=$2; shift 2;;
  *)break;;
esac
done

ft $BASE_APP_PATH$INSTANCE

#config tomcat
export JPDA_ADDRESS=${JPDA_PORT}
export CATALINA_BASE="/data/www/servers/${INSTANCE}"
export CATALINA_HOME="/home/tomcat/tomcat7"
export JVM_OPTIONS="-Xms128m -Xmx1024m -XX:permSize=128 -XX:MaxPermSize=512m"
BIN="/bin/catalina.sh"
BIN_PATH=$CATALINA_HOME$BIN

source /etc/profile

function stopserver ()
{
  startport=`lsof -i :$2|grep -v "PID"|awk '{print $2}'`
  if [ "$startport" != "" ]; then
    $1 stop
    sleep 5s
  fi
}
#localhost_path, appBase, app_name
function createNewAppConfig ()
{
  path=`pwd`
  cd $1
  for file in `ls`
  do
    rm $file
  done
  file="${3}.xml"
  touch file
  echo "<Context path=\"/${3}\" docBase=\"${2}/${3}\" reload=\"true\"></Context>" >> $file
  cd $path
}

#config path
function copyConfig ()
{
  if [ -f "${1}/log4j.xml" ]; then
    echo "${1}/log4j.xml"
    cp ${1}/log4j.xml ${2}/WEB-INF/classes/log4j.xml
  fi
  if [ -f "${1}/jdbc.properties" ]; then
    echo "${1}/jdbc.properties"
    cp ${1}/jdbc.properties ${2}/WEB-INF/classes/jdbc.properties
  fi
}

#if restart ,no git pull and compile
if [ "$RESTART" = 1 ]; then
  echo "================>${BIN_PATH} stop<===================="
  stopserver $BIN_PATH $APP_PORT
  let "idx-=1"
  appBase=${BASE_APP_PATH}${INSTANCE}/${currentDay}${idx}
  copyConfig /data/www/conf/${APP_NAME}/${INSTANCE} ${appBase}/${APP_NAME}
  createNewAppConfig ${CATALINA_BASE}/conf/Catalina/localhost ${appBase} ${APP_NAME}
  if [ "$JPDA" = 1 ]; then
    echo "=====================>${BIN_PATH} jpda start<========================="
    $BIN_PATH jpda start
  else
    echo "=====================>${BIN_PATH} start<==========================="
    $BIN_PATH start
  fi
elif [ "$COMMAND" = "start" ]; then
  let "idx-=1"
  #modify serverx.xml webapps
  appBase=${BASE_APP_PATH}${INSTANCE}/${currentDay}${idx}
  copyConfig /data/www/conf/${APP_NAME}/${INSTANCE} ${appBase}/${APP_NAME}
  createNewAppConfig ${CATALINA_BASE}/conf/Catalina/localhost ${appBase} ${APP_NAME}
  if [ "$JPDA" = 1 ]; then
    echo "=====================>${BIN_PATH} jpda start<========================="
    $BIN_PATH jpda start
  else
    echo "=====================>${BIN_PATH} start<==========================="
    $BIN_PATH start
  fi
elif [ "$COMMAND" = "stop" ]; then
  echo "================>${BIN_PATH} $APP_PORT stop<===================="
  stopserver $BIN_PATH $APP_PORT
elif [ "$COMMAND" = "deploy" ]; then
  cd $BASE_SRC
  cd $SRC
  git pull
  git checkout $BRANCH
  mvn -Dmaven.test.skip=false clean package -Ptest -X
  appBase=${BASE_APP_PATH}${INSTANCE}/${currentDay}${idx}
  mkdir $appBase
  cp /data/www/deploy/${APP_NAME}.war ${appBase}/${APP_NAME}.war
  unzip ${appBase}/${APP_NAME}.war -d ${appBase}/${APP_NAME}
  rm ${appBase}/${APP_NAME}.war
  copyConfig /data/www/conf/${APP_NAME}/${INSTANCE} ${appBase}/${APP_NAME}
  createNewAppConfig ${CATALINA_BASE}/conf/Catalina/localhost ${appBase} ${APP_NAME}  
  cd /data/www/shell
  stopserver $BIN_PATH $APP_PORT
  if [ "$JPDA" = 1 ]; then
    echo "=====================>${BIN_PATH} jpda start<========================="
    $BIN_PATH jpda start
  else
    echo "=====================>${BIN_PATH} start<==========================="
    $BIN_PATH start
  fi
fi
echo "==============================="

