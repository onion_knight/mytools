SQL--case when用来汇总数据
select 
	count(distinct case when pay_merchant_flag = 1 and group_id=0 then `merchant_id`
	else NULL
	end ) as 'aaaa',
	
	count(distinct case when new_merchant_flag = 1 and group_id=0 then `merchant_id`
	else NULL
	end ) as 'bbbb'
from 
	rp_crm_merchant_sale_d
where 
	 `day_key` = '20160801'

select 
	sum(case when day_key = '20160801' then pay_order_amt
	else 0 
	end) as 'a',
	
	sum(case when day_key = '20160802' then pay_order_amt
	else 0 
	end) as 'b'
from 
	rp_crm_merchant_sale_d
where 
	day_key >= '20160801'
	

-- 由crm的t_product表插入数据到shanghu的sh_server服务表
	INSERT INTO shanghu.sh_server (
	store_id,
	channel,
	singed_product_id,
	server_code,
	orig_server_code,
	server_name,
	server_price,
	work_hour_price,
	work_hour,
	category_code,
	category_name,
	create_person,
	create_time,
	update_person,
	update_time
) SELECT
	p.STORE_ID AS store_id,
	1 AS channel,
	p.id AS singed_product_id,
	c.CATEGORY_CODE AS server_code,
	c.ORIG_CATEGORY_CODE as orig_server_code,
	c.CATEGORY_NAME AS server_name,
	p.CLEAR_AMT AS server_price,
	0 AS work_hour_price,
	0 AS work_hour,
	c1.CATEGORY_CODE AS category_code,
	c1.CATEGORY_NAME AS category_name,
	'system' AS create_person,
	NOW() AS create_time,
	'system' AS update_person,
	NOW() AS update_time
FROM
	crm.t_product p
INNER JOIN crm.t_item_category c ON p.CATEGORY_CODE = c.CATEGORY_CODE
INNER JOIN crm.t_item_category c1 ON c.PARENT_ID = c1.ID
WHERE
	p.is_online=1 
and	p.STORE_ID = 20072;
	
-- 查询工单列表和服务 关键词--GROUP_CONCAT
        SELECT
            w.id, w.store_id, w.work_no, w.store_order_id, w.status, w.create_person, w.create_time,
            o.type, o.status o_status, o.channel, o.car_id, o.user_id, o.start_time, o.adviser_id,
            o.sale_cost, o.real_amount, o.charge_amount,
            a.category_codes, a.category_names
        FROM
            sh_work w
        LEFT JOIN sh_order o ON w.store_order_id = o.id
        LEFT JOIN (
            SELECT
                s.order_id,
                GROUP_CONCAT(DISTINCT(s.category_code)) AS "category_codes",
                GROUP_CONCAT(DISTINCT(s.category_name)) AS "category_names"
            FROM
                sh_order_server s
            WHERE
                s.store_id = 99
            GROUP BY
                s.order_id
        ) a ON o.id = a.order_id
		
		
-- 查询门店服务用户
SELECT  GROUP_CONCAT(DISTINCT(ui.`name`)) as 'name', GROUP_CONCAT(DISTINCT(ui.phone)) as phone, 
count(1), sum(o.sale_cost)/100 as total_sale_cost, sum(o.real_amount)/100 as total_real_amount,
GROUP_CONCAT(DISTINCT(substring_index(uc.car_name,' ',2))) as car_name, GROUP_CONCAT(DISTINCT(uc.car_name)) as car_full_name
FROM sh_work w 
INNER JOIN sh_order o ON w.store_order_id = o.id
LEFT JOIN sh_user_info ui ON o.user_id=ui.id
LEFT JOIN sh_user_car uc ON o.car_id=uc.id
WHERE o.store_id=9275 and w.`status`!=7 and o.type=2
GROUP BY o.user_id;



-- navicat SQL事务 可用于平时测试数据然后回滚
begin;
insert into sh_temp(store_id, store_server_id) values(1,1);
select * from sh_temp;
rollback;
select * from sh_temp;


BEGIN
DECLARE i INT;
START TRANSACTION;
SET i=0;
WHILE i<1000000 DO
 INSERT INTO t_user VALUES(NULL,CONCAT(i+1,'@xxg.com'),i+1);
 SET i=i+1;
END WHILE;
COMMIT;
END

