我的其他整理：

最终榜单

这榜单阵容太豪华了，后几名都是如此有用，毫不示弱。

?  Top #10切来切去：Ctrl+Tab
?  Top #9选你所想：Ctrl+W
?  Top #8代码生成：Template/Postfix +Tab
?  Top #7发号施令：Ctrl+Shift+A
?  Top #6无处藏身：Shift+Shift
?  Top #5自动完成：Ctrl+Shift+Enter
?  Top #4创造万物：Alt+Insert 新建：Alt+Insert可以新建类、方法等任何东西。

太难割舍，前三名并列吧！
?  Top #1智能补全：Ctrl+Shift+Space  || Alt + /
?  Top #1自我修复：Alt+Enter
?  Top #1重构一切：Ctrl+Shift+Alt+T


Junit test插件配置修改：生成的test类位置
${SOURCEPATH}/../../test/java/${PACKAGE}/${FILENAME}


------------------------一些小技巧

……输入iter+按tab,则生成foreach...

……打开pom文件，右键-->show diagram 就能看到jar包依赖关系图

……突然发现idea 错误不提示，解决办法：菜单 File-->power save mode(省电模式)不要选中。如果本身就处于未选中状态，则先选中，再取消

……Maven工程在使用snapshots版本开发时，在Setting-->Maven中选中 Always update snapshots||如果不管用




乱码：
控制台及日志乱码解决：设置[tomcat] server的VM options为-Dfile.encoding=UTF-8；IDE的bin\idea.exe.vmoptions目录添加-Dfile.encoding=UTF-8

tomcat:
intelli idea中实际tomcat配置文件路径 C:\Users\Administrator\.IntelliJIdea15\system\tomcat

激活码：
http://www.iteblog.com/idea/ 在线免费生成IntelliJ IDEA 15.0注册码
License server，直接输入http://www.iteblog.com/idea/key.php地址即可激活IntelliJ IDEA 15
http://idea.iteblog.com/key.php --目前用的激活网址

快捷键：
查看层级 选中类按F4=Type Hierarchy
ctrl +alt +t ==suround with
ctrl + shift + space = 查看document注释  shift+esc关闭
CTRL+E 打开最近编辑的文件
快捷键 shift+enter 在当前行的下一行插入空白行
查看doc ctrl+shirt+space

插件：
jrebel 插件实现服务热部署runtimeorg.activiti.rest.service.api.repository.DeploymentResource

一、maven项目管理：
新建目录，将maven目录checkout出来
设置maven环境
设置tomcat环境
设置java环境
设置hosts：增加127.0.0.1 localhost 否则run 启动后停止时 java进程还在

idea 新增工作目录 需要设置jdk
MAVEN VM 选项设置 -Dmaven.test.skip=true -Dmaven.multiModuleProjectDirectory=$M2_HOME


二、安装插件


Ace Jump插件 

可以说Ace Jump和IdeaVim这两个插件是我使用了Intellij后再也不想用eclipse的最主要原因。Ace Jump是一种从emacs上借鉴过来的快速光标跳转方式，
操作方式是：你用某个快捷键进入Ace Jump模式后，再按任一个键，当前屏幕中所有该字符都被打上一个字母标记，你只要按这个字母，光标就会跳转到这个字符上。
这种跳转方式非常实用，你根本不用管当前光标在什么位置，眼睛只需要盯着需要跳转到的位置，最多三四下按键就能准确把光标定位，开始编辑。按道理这种功能
非常容易实现，但偏偏到目前为止我没有在eclipse上找到类似插件。

如果选择了AceJump插件，重启Intellij后即可使用，默认快捷键是Ctrl+；（分号）。
在AceJump模式下，按下N键，屏幕可见范围内的字符N均被打上标记，按相应字母即可跳转到该位置。 

三. Shortcut Translator插件 

从其他IDE转到Intellij时学习快捷键的插件。安装后，按Ctrl+Shift+K调出快捷键翻译对话框，选定你惯用的IDE keymap和需要学习的keymap，按下惯用keymap的快捷键，
即可看到学习keymap上的对应快捷键。 

四、常用配置

1.界面中文方框问题

Settings->Appearance中

Theme 设置 Windows

2.显示行号

Settings->Editor->Appearance标签项，勾选Show line numbers

3.光标不随意定位

Settings->Editor中去掉Allow placement of caret after end of line。

4.启动的时候不打开工程文件

Settings->General去掉Reopen last project on startup.

5.无法起动Tomcat（  IntelliJ IDEA ）

请使用ZIP版的Tomcat


8.用*标识编辑过的文件

Editor –> Editor Tabs 选中Mark modifyied tabs with asterisk 


9.编码设置：编辑器中中文乱码问题

这个是项目字符编码设置错误

FILE ->Settings->       有3处设置根据自己需要设置

IDE Encondings：IDE编码 ，选择 IDE Encoding为GBK。这边要自己去调整了

Project Encoding：项目编码

Default encoding for properties files：默认文件编码

10.编辑器中字体和大小

FILE -> Settings> Editor->Colors & Fonts -> Font -> 右侧

Size：字体大小

Line spacing：行间距

11.显示文件夹或文件过滤

File->Setting->File Types->Ignore file and folders

里面填写你要的过滤不显示的

注意大小写哦

12.当前编辑文件定位

方法1) 在编辑的所选文件按ALT+F1, 然后选择PROJECT VIEW
方法2) 左侧 项目列表框 顶部的 定位图标 圆圈中间一个叉


13.优化文件保存

File->Settings->General->

Synchronize file on frame activation：个人需要是否取消同步文件

Save files on framedeactivation：取消

Save files automatically：选中，设置自动保存，设置 30秒自动保存时间，这样IDEA依然可以自动保持文件,所以在每次切换时，你需要按下Ctrl+S保存文件


14.Spring Configuration Check解决办法
IntelliJ > Preferences > Appearance & Behavior > Notifications > Spring Configuration Check

8.取消代码拼写检查
Settings->Inspections > Spelling > Typo

9.tab转空格
file - settings - code style - general - tab size 

五、快捷键

代码提示快捷键设置
Main menu--Code--Completion--Basic+SmartType

基本的代码提示用Ctrl+Space
Ctrl+Shift+Enter就能自动补全末尾的字符
**************** 用F2/ Shift+F2移动到有错误的代码
Alt+Enter快速修复
用Ctrl+J可以查看所有模板

TAB代码补全
自动按语法选中代码的Ctrl+W以及反向的Ctrl+Shift+W

Ctrl+Left/Right移动光标到前/后单词
Ctrl+[/]移动到前/后代码块
Alt+方向：移动到前/后方法
非常普通的像Ctrl+Y删除行、Ctrl+D复制行、Ctrl+？？？折叠代码

************Ctrl+N/Ctrl+Shift+N可以打开类或资源
************Search Everywhere功能，只需按Shift+Shift

************类的继承关系则可用Ctrl+H打开类层次窗口

************在继承层次上跳转则用Ctrl+B/Ctrl+Alt+B分别对应父类或父方法定义和子类或子方法实现，

************查看当前类的所有方法用Ctrl+F12
************要找类或方法的使用也很简单，Alt+F7
************Ctrl+Alt+Y 同步
************CTRL+Q   显示注释文档


要查找文本的出现位置就用Ctrl+F/Ctrl+Shift+F在当前窗口或全工程中查找，再配合F3/Shift+F3前后移动到下一匹配处。

命令：Ctrl+Shift+A可以查找所有Intellij的命令，并且每个命令后面还有其快捷键。所以它不仅是一大神键，也是查找学习快捷键的工具。

格式化代码：格式化import列表Ctrl+Alt+O，格式化代码Ctrl+Alt+L。

运行：Alt+Shift+F10运行程序，Shift+F9启动调试，Ctrl+F2停止。

F7/F8/F9分别对应Step into，Step over，Continue

单元测试：Ctrl+Alt+T创建单元测试用例。

切换窗口：Alt+Num，常用的有1-项目结构，3-搜索结果，4/5-运行调试。Ctrl+Tab切换标签页，Ctrl+E/Ctrl+Shift+E打开最近打开过的或编辑过的文件。

例如水平分屏Ctrl+|等，和一些神奇的小功能Ctrl+Shift+V粘贴很早以前拷贝过的，Alt+Shift+Insert进入到列模式进行按列选中。

alt+回车 导入包,自动修正

ctrl+shift+f12 窗口最大化

Ctrl-Shift-V-----------------最近使用的剪贴板内容

Alt+Insert 生成代码(如get,set方法,构造函数等)

Ctrl-Shift-J-----------------两行合成一行

Ctrl+Shift+/ 加上或取消段注释/**/

Ctrl+Alt+O 优化导入的类和包

shift+esc 隐藏边栏

svn check in atl+i
svn update atl + u
svn compare atl + c

打开调试窗口 alt+p
重启服务 ctrl+f12

打开maven project alt+m

alt+7 显示类方法

Ctrl+E或者Alt+Shift+C  最近更改的代码
Ctrl+R 替换文本
Ctrl+F 查找文本
Ctrl+Shift+Space 自动补全代码
Ctrl+空格 代码提示
Ctrl+Alt+Space 类名或接口名提示
Ctrl+P 方法参数提示

Ctrl+Shift+Alt+N 查找类中的方法或变量
Alt+Shift+C 对比最近修改的代码
 
Shift+F6  重构-重命名
Ctrl+Shift+先上键
Ctrl+X 删除行
Ctrl+D 复制行
Ctrl+/ 或 Ctrl+Shift+/  注释（// 或者/*...*/ ）
Ctrl+J  自动代码
Ctrl+E 最近打开的文件
Ctrl+H 显示类结构图
Ctrl+Q 显示注释文档
Alt+F1 查找代码所在位置
Alt+1 快速打开或隐藏工程面板
Ctrl+Alt+ left/right 返回至上次浏览的位置
Alt+ left/right 切换代码视图
Alt+ Up/Down 在方法间快速移动定位
Ctrl+Shift+Up/Down 代码向上/下移动。
F2 或Shift+F2 高亮错误或警告快速定位
 
代码标签输入完成后，按Tab，生成代码。
选中文本，按Ctrl+Shift+F7 ，高亮显示所有该文本，按Esc高亮消失。
Ctrl+W 选中代码，连续按会有其他效果
选中文本，按Alt+F3 ，逐个往下查找相同文本，并高亮显示。
Ctrl+Up/Down 光标跳转到第一行或最后一行下
Ctrl+B 快速打开光标处的类或方法 



六、常用技能

1、idea 打包jar：

1） 选中Java项目工程名称，在菜单中选择 File->project structure... (快捷键Ctrl+Alt+Shift+S)。
2） 在弹出的窗口中左侧选中"Artifacts"，点击"+"选择jar，然后选择"from modules with dependencies"。
3） 在配置窗口中配置"Main Class"。
4） 选择“Main Class”后，选择“copy to the output  and link via manifest”，配置“Directory for META-INF/MAINFEST.MF”，此项配置的缺省值是：D:\workshop\DbUtil\src\main\java，需要改成：D:\workshop\DbUtil\src\main\resources，如果不这样修改，打成的jar包里没有包含META-INF/MAINFEST.MF文件，这个应该是个IDEA的BUG（参考：http://stackoverflow.com/questions/15724091/how-to-run-a-jar-file-created-using-intellij-12），配置完成后如下图所示，点击OK进入下一步。（如果选择“extract to the target jar”，即把第三方jar文件，打入最终的可运行jar包时，可以不修改“Directory for META-INF/MAINFEST.MF”的配置，用缺省值即可）

2、创建多模块maven项目

MAVEN VM 选项设置 -Dmaven.multiModuleProjectDirectory=$M2_HOME

3、设置idea临时文件目录：
idea.properties 用记事本或者Editplus 将其打开，找到如下代码段：
[html] view plain copy
# path to IDEA config folder. Make sure you're using forward slashes  
idea.config.path=${user.home}/.IntelliJIdea90/config  改为D:/Java/apache-maven-3.3.3/temp/.IntelliJIdea14
  
# path to IDEA system folder. Make sure you're using forward slashes  
idea.system.path=${user.home}/.IntelliJIdea90/system   改为D:/Java/apache-maven-3.3.3/temp/.IntelliJIdea14

4、添加依赖包


5、Intellij修改archetype Plugin配置


6、创建自己的archetype

一  mvn archetype:genetate 创建一个标准的maven工程

    添加自己想要重复利用的元素，如一些配置，通用代码等。

二 在工程下执行mvn archetype:create-from-project
    此时会生成 target/generated-sources/archetype
    进入archetype目录下，执行mvn install

三 此时 mvn archetype:genetate时就可以选择自己的archetype进行创建了

    当然，本地的 archetypeCatolog一般为local，所以可以这样：mvn archetype:genetate -DarchetypeCatalog=local,再选择catalog下具体的archetype


8.设置idea的SVN忽略掉*.iml文件
Editor->File Types=>Ignore files and folders增加*.iml;


9.远程调试时，启动时端口需要一致
Remote Connection Setting 端口设置为远端的address端口
“启动/连接”选项，port设置为远端的address端口

10、资源文件如何实时更新？
在配置Tomcat Server时，
设置“On Update Action”为Redeploy
“On frame deactivation”为update class and resource


