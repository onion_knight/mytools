python

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


https://www.pythonanywhere.com/
应该是个python服务器，可以在线编辑并提交代码，让服务器一直运行，but收费的

python setup.py install -- 通过setup.py安装
example:
pip install redis---通过pip安装redis模块


r''表示''内部的字符串默认不转义
'''...'''的格式表示多行内容
Python中，通常用全部大写的变量名表示常量	PI = 3.14159265359
Python除法 10/3结果是浮点数 10//3结果取整 10%3取余数

常见的占位符有：
%d	整数
%f	浮点数
%s	字符串
%x	十六进制整数

用len()函数可以获得list元素的个数
#!/usr/bin/env python3  ---linux解析python模块

tuple 元组  classmates = ('Michael', 'Bob', 'Tracy')---一旦初始化就不能修改
list 数组  classmates = ['Michael', 'Bob', 'Tracy']
dict 字典=map 
set

函数：
list(range(5)) -- 生成数组1..5
int(str) --- str to int
float、str、bool

x=b'...'
x.decode('utf-8') 字节转字符

函数参数 位置参数,默认参数,可变长参数,关键字参数,命名关键字参数 共5种
		*a  a为可变长参数
		**a a为关键字参数 def person(name, age, **kw):
		>>> extra = {'city': 'Beijing', 'job': 'Engineer'}
		>>> person('Jack', 24, **extra)
		**extra表示把extra这个dict的所有key-value用关键字参数传入到函数 注意kw获得的dict是extra的一份拷贝，对kw的改动不会影响到函数外的extra
		def person(name, age, *, city, job): 只接收city和job作为关键字参数。命名关键字参数需要一个特殊分隔符*，*后面的参数被视为命名关键字参数

在Python中定义函数，可以用必选参数、默认参数、可变参数、关键字参数和命名关键字参数，这5种参数都可以组合使用，除了可变参数无法和命名关键字参数混合
参数定义的顺序必须是：必选参数、默认参数、可变参数/命名关键字参数和关键字参数。 
def f1(a, b, c=0, *args, **kw) 
def f2(a, b, c=0, *, d, **kw)
对于任意函数，都可以通过类似func(*args, **kw)的形式调用它，无论它的参数是如何定义的。
*args是可变参数，args接收的是一个tuple；

**kw是关键字参数，kw接收的是一个dict

pass---让程序正常流走

python reload(module)  先import imp

L[0:10:2] list切片（分割）
字符串'xxx'也可以看成是一种list，每个元素就是一个字符。因此，字符串也可以用切片操作，只是操作结果仍是字符串
 for i, value in enumerate(['A', 'B', 'C']):
 [m + n for m in 'ABC' for n in 'XYZ']
 [d for d in os.listdir('.')]  列出当前目录下的所有文件和目录名
  for k, v in d.items():
	  print(k, '=', v) ##dict迭代
如果一个函数定义中包含yield关键字，那么这个函数就不再是一个普通函数，而是一个generator


python 高阶函数  即函数作为参数传入

int2 = functools.partial(int, base=2) --- 偏函数

函数--通过yield x 生成generate

filter(fn, list) 返回iterator

python的yield n 可以返回无限序列（惰性）

sorted(list) sorted(list,key=abs,reverse=True) 自定义排序

python 用@func 装饰器--类似注解

hasattr(f,'__call__')判断f是否为函数(setattr/hasattr) callable(fn)  type(arg) == str    if isinstance(arg0,(str,int,float)) 
type()函数返回对应的Class类型 type(fn)==types.FunctionType type(abs)==types.BuiltinFunctionType
如果要获得一个对象的所有属性和方法，可以使用dir()函数
这种写法：print(1) if n1==1 else print(2)

正常的函数和变量名是公开的（public），可以被直接引用，比如：abc，x123，PI等；
类似__xxx__这样的变量是特殊变量，可以被直接引用，但是有特殊用途，比如上面的__author__，__name__就是特殊变量 我们自己的变量一般不要用这种变量名；
类似_xxx和__xxx这样的函数或变量就是非公开的（private），不应该被直接引用，比如_abc，__abc等；

特殊的__slots__变量，来限制该class实例能添加的属性

@property/@score.setter =get/set

有的错误类型都继承自BaseException
raise xxError--抛出异常
try:
	...
except xx as e:
	...
else:#如果没有except则try之后会执行else
	...
finally:
	...
assert n!=0  启动Python解释器时可以用-O参数来关闭assert($ python3 -O err.py)
Python的调试器pdb （$ python3 -m pdb err.py）
import pdb  pdb.set_trace() # 运行到这里会自动暂停
然用IDE调试起来比较方便，但是最后你会发现，logging才是终极武器

IO:
with open('/path/to/file', 'r') as f: ---相当于finally: f.close()
    print(f.read())
StringIO
BytesIO
# 查看当前目录的绝对路径:
>>> os.path.abspath('.')
'/Users/michael'
# 在某个目录下创建一个新目录，首先把新目录的完整路径表示出来:
>>> os.path.join('/Users/michael', 'testdir')
'/Users/michael/testdir'
# 然后创建一个目录:
>>> os.mkdir('/Users/michael/testdir')
# 删掉一个目录:
>>> os.rmdir('/Users/michael/testdir')

os.path.isdir(x)

全局变量定义
balance = 0
def change_it(n):
    global balance

常用代码：
args = sys.argv
if __name__=='__main__'

常用模块：
sys logging time threading
Pillow -- 图像处理

JSON类型 Python类型
{}	dict
[]	list
"string"	str
1234.56	int或float
true/false	True/False
null	None


pip install modules:
	requests/beautifulsoup4

