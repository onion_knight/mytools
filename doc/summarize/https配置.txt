1、生成keystore
keytool -v -genkey -alias tomcat -keyalg RSA -keystore d:/tomcat.keystore  -validity 36500

输入keystore密码：123456
输入名字、组织单位、组织、市、省、国家等信息
注意事项：
A、Enter keystore password：此处需要输入大于6个字符的字符串 123456
B、“What is your first and last name?”这是必填项，并且必须是TOMCAT部署主机的域名或者IP[如：gbcom.com 或者 10.1.25.251]，就是你将来要在浏览器中输入的访问地址
C、“What is the name of your organizational unit?”、“What is the name of your organization?”、“What is the name of your City or Locality?”、“What is the name of your State or Province?”、“What is the two-letter country code for this unit?”
可以按照需要填写也可以不填写直接回车，在系统询问“correct?”时，对照输入信息，如果符合要求则使用键盘输入字母“y”，
否则输入“n”重新填写上面的信息
D、Enter key password for <tomcat>，123456 这项较为重要，会在tomcat配置文件中使用，建议输入与keystore的密码一致，设置其它密码也可以
l  完成上述输入后，直接回车则在你在第二步中定义的位置找到生成的文件

2、进入tomcat文件夹 
找到conf目录下的sever.xml并进行编辑
<Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true" 
     maxThreads="150" scheme="https" secure="true" 
     clientAuth="false" keystoreFile="D:/AppServer/Tomcat/apache-tomcat-6.0.32/conf/tomcat.keystore" 
     keystorePass="deleiguo" sslProtocol="TLS" /> 
     
     方框中的keystore的密码，就是刚才我们设置的“123456”.
     
     
3、Tomcat启动成功后，使用https://127.0.0.1:8443 访问页面

页面成功打开即tomcat下的https配置成功。

 

4、应用程序HTTP自动跳转到HTTPS

在应用程序中web.xml中加入：

<security-constraint> 
       <web-resource-collection > 
              <web-resource-name >SSL</web-resource-name> 
              <url-pattern>/*</url-pattern> 
       </web-resource-collection>
                             
       <user-data-constraint> 
              <transport-guarantee>CONFIDENTIAL</transport-guarantee> 
       </user-data-constraint> 
</security-constraint>
 
5、生成安全证书文件
keytool -export -alias tomcat -file D:/file.cer -keystore d:/tomcat.keystore -validity 36500
然后输入d:/tomcat.keystore中的keystore密码
 
-file D:/file.cer 即为生成的cer文件，可直接点击安装
 
6、注意事项：
（1）    生成证书的时间，如果IE客户端所在机器的时间早于证书生效时间，或者晚于有效时间，IE会提示“该安全证书已到期或还未生效”
（2）    如果IE提示“安全证书上的名称无效或者与站点名称不匹配”，则是由生成证书时填写的服务器所在主机的域名“您的名字与姓氏是什么？”/“What is your first and last name?”不正确引起的
 
7、遗留问题：
（1）如果AC主机不能通过域名查找，必须使用IP，但是这个IP只有在配置后才能确定，这样证书就必须在AC确定IP地址后才能生成
（2）证书文件只能绑定一个IP地址，假设有10.1.25.250 和 192.168.1.250 两个IP地址，在证书生成文件时，如使用了10.1.25.250，通过IE就只能使用10.1.25.250 来访问AC-WEB，192.168.1.250是无法访问AC-WEB的。
posted on 2012-09-26 16:34 abin 阅读(9138) 评论(0)  编辑  收藏 所属分类: HTTPS

